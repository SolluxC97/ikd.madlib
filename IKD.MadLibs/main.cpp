
// MadLib
// Team 1

#include <iostream>
#include <conio.h>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

void UserInput(string *iArray);
void PrintInput(string *iArray, string *cArray);
void SaveToFile(string *iArray, string *cArray);

int main()
{
	string inputs[14];
	string content[14] =
	{
		"It was a ",
		", cold November day. I woke up to the  \n",
		" smell of ",
		" roasting in the ",
		" downstairs. I ",
		" down the stairs to see if I could help  \n",
		" the dinner. My mom said, 'See if ",
		" needs a fresh ",
		" 'So I carried a tray of glasses full of  \n",
		" into the ",
		" room. When I got there, I couldn't believe my ",
		"! There were ",
		" on the ",
		"! "
	};

	UserInput(inputs);
	cout << "\n";
	PrintInput(inputs, content);
	cout << "\n";
	SaveToFile(inputs, content);
	cout << "\n";

	(void)_getch();
	return 0;
}

void UserInput(string *iArray)
{

	string prompts[14] =
	{
		 "Enter an adjective: ",
		 "Enter an adjective: ",
		 "Enter a type of bird: ",
		 "Enter a room in a house: ",
		 "Enter a past tense verb: ",
		 "Enter a verb: ",
		 "Enter a relative's name: ",
		 "Enter a noun: ",
		 "Enter a liquid: ",
		 "Enter a verb ending in 'ing': ",
		 "Enter parts of the body: ",
		 "Enter a plural noun: ",
		 "Enter a verb ending in 'ing': ",
		 "Enter a noun: "
	};

	for (int i = 0; i < 14; i++)
	{

		cout << prompts[i];
		cin >> iArray[i];

	}
}

void PrintInput(string *iArray, string *cArray)
{

	for (int i = 0; i < 14; i++)
	{
		cout << cArray[i] << iArray[i];
	}
}

void SaveToFile(string *iArray, string *cArray)
{
	char input;

	cout << "Would you like to save output to file? (y/n): ";
	cin >> input;

	if (input == 'y' || input == 'Y')
	{

		ofstream file;
		file.open("MadLib.txt");
		file << cArray[0] << iArray[0];
		file << cArray[1] << iArray[1];
		file << cArray[2] << iArray[2];
		file << cArray[3] << iArray[3];
		file << cArray[4] << iArray[4];
		file << cArray[5] << iArray[5];
		file << cArray[6] << iArray[6];
		file << cArray[7] << iArray[7];
		file << cArray[8] << iArray[8];
		file << cArray[9] << iArray[9];
		file << cArray[10] << iArray[10];
		file << cArray[11] << iArray[11];
		file << cArray[12] << iArray[12];
		file << cArray[13] << iArray[13];
		file.close();
		cout << "Mad Lib has been saved to text file. \n";
	}
}